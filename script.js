const WIDTH_CUSHION = 100
const SPEED = 15
const SCALE = 0.17
let canvas
let ctx

var choices = ["😗 i love balca 😠", "🥹 i love you so much wifey 🥹"]

function randomStr() {
  return choices[Math.floor(Math.random() * choices.length)]
}

const emoji = {
  src: randomStr(), // choices[0],
  x: -50,
  y: 100,
  xSpeed: 10,
  ySpeed: 10,
  yShift: 100,
  yDirection: 1,
  xDirection: 1,
}

;(function main() {
  canvas = document.getElementById("canv")
  canvas.width = window.innerWidth
  canvas.height = window.innerHeight
  ctx = canvas.getContext("2d")
  ctx.font = "100px sans-serif"
  // ctx.font
  ctx.fillText(emoji.src, emoji.x, emoji.y)
  update()
})()

function update() {
  setTimeout(() => {
    ctx.fillStyle = "white"
    ctx.fillRect(0, 0, canvas.width, canvas.height)

    ctx.fillStyle = "black"
    ctx.fillText(emoji.src, emoji.x, emoji.y)

    movement()
    update()
  }, SPEED)
}

function movement() {
  if (emoji.x > canvas.width + WIDTH_CUSHION) {
    emoji.xDirection = -1
  } else if (emoji.x < 0 - ctx.measureText(emoji.src).width) {
    emoji.xDirection = 1
  }

  if (emoji.y > canvas.height) {
    emoji.yDirection = -1
  } else if (emoji.y < 0) {
    emoji.yDirection = 1
  }

  if (emoji.x > canvas.width + WIDTH_CUSHION || emoji.x < 0 - ctx.measureText(emoji.src).width) {
    emoji.y += emoji.yShift * emoji.yDirection
  }
  emoji.x += emoji.xSpeed * emoji.xDirection
  emoji.xSpeed += 0.005
  if (
    (emoji.x > canvas.width || emoji.x < 0 - ctx.measureText(emoji.src).width) &&
    (emoji.y > canvas.height || emoji.y < 0)
  ) {
    emoji.src = randomStr()
  }
}
